const Course = require("../models/Course.js");

//Mini-Activity

module.exports.addCourse = (checkAdmin, requestBody) => {
	console.log(checkAdmin.isAdmin);
	if(checkAdmin.isAdmin == true){
		let newCourse = new Course({
			name: requestBody.name,
			description: requestBody.description,
			price: requestBody.price,
		});

		return newCourse.save().then((course,error) => {	
			if(error){
				return false;
			}else{
				return true;
			};
		});
	}else{
		return Course.find(checkAdmin).then((error) => {
			return false;
		});
	}
}

module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	});
};


module.exports.getAllActive = (isActive) => {
	return Course.find({isActive: true}).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}else{
			return result;
		};
	});
};

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}else{
			return result;
		};
	});
};


module.exports.updateCourse = (reqParams,reqBody) => {
	let updateCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	return Course.findByIdAndUpdate(reqParams.courseId, updateCourse).then((course,error) => {
		if(error){
			return false;
		} else{
			return true;
		}
	})
};


module.exports.archiveCourse = (reqParams) => {
	let updateActiveField = {
		isActive: false
	}
	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course,error) => {
		if(error){
			return false;
		} else{
			return true;
		}
	})
};