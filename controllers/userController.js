const User = require("../models/User.js");
const Course = require("../models/Course.js");

const auth = require("../auth.js")


const bcrypt = require("bcrypt");




module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			//if there is an existing duplicate email
			return true;
		}else{
			//if the user email is not yet registered in our database
			return false;
		}
	});
};

//User Registration
module.exports.registerUser = (requestBody) => {
	let newUser = new User({
		firstName: requestBody.firstName,
		lastName: requestBody.lastName,
		email: requestBody.email,
		mobileNo: requestBody.mobileNo,
		//hashSync - bcrypt's method for encrypting the password of the user once they have successfully registered in our database
		/*
			firstParameter -the value to which encryption will be done, password coming from the request body
			"10" - dictates how may "salt" rounds are to be given to encrypt the value
		*/
		password: bcrypt.hashSync(requestBody.password,10)
	});
	//waits for the save method to complete before detecting any errors
	return newUser.save().then((user,error) => {
		if(error){
			console.log(error);
			return false;
		}else{
			return true;
		};
	});
};

//User Login
/*
	1.check the database of the user email exists
	2.compare the password provided in the request body with the password stored in the database
	3.generate/return a JSON web token if the user had successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) =>{
	return User.findOne({email: reqBody.email}).then(result => {
		if(result.length === null){
			//if the user does not exist
			return false;
			//if the user email exist in the database
		}else{
			//compareSync = decodes the encrypted password form the database and compares it to the password received from the request body
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password);

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)};
			}else{
				return false;
			}
		}
	});
}

//ACTIVITY - Create a /details route that will accept the user’s Id to retrieve the details of a user.


 	module.exports.getProfile = (reqId) => {
 		return User.findOne(reqId).then((result, error) => {
 			if(error){
 				console.log(error);
 				return false;
 			}else{			
 				result.password = "";
 				return result
 			};
 		});
 	};




//enroll a user to a class
//async await will be used in enrolling since we have two documents to be updated in our database: user document and course document
module.exports.enroll = async (data) =>{
	//adding the courseId in the enrollments array of the user
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({courseId: data.courseId});
		return user.save().then((user,error) =>{
			if(error){
				return false;
			}else{
				return true;
			}
		})
	})
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({userId: data.userId});
		return course.save().then((course,error) =>{
			if(error){
				return false;
			}else{
				return true;
			}
		})
	})

	if(isUserUpdated && isCourseUpdated){
		//User enrollment successful
		return true;
	}else{
		//User enrollment failed
		return false;
	}
}

/*
Find By ID:

	router.get('/:id', (req, res) => {
		const { id } = req.params;

		const foundUser = users.find((user) => user.id === id);

		res.send(foundUser);
	})

Delete By ID:
	
	router.delete('/:id', (req, res) => {
	const { id } = req.params;

	users. users.filter((user) => user.id !== id);

	res.send(`User with the id ${id} deleted from the database`)

	})

Update specific ID:

	router.patch('/:id', (req, res) => {
	const { id } = req.params;
	const { firstName, lastName, age } = req.body;


	const userToBeUpdated = users.find((user) => users.id === id);

	if(firstName) user.firstName = firstName;
	if(lastName) user.lastName = lastName;
	if(age) user.age = age;
	
	res.send(`User with the ID ${id} has been updated`)
	})
*/