const express = require("express");

const mongoose = require("mongoose");


//allows us to control the app's Cross-Origin Resouce Sharing settings
const cors = require("cors");


//routes
const userRoutes = require("./routes/userRoutes.js")
const courseRoutes = require("./routes/courseRoutes.js")

const app = express();

//MongoDB Connection
mongoose.connect("mongodb+srv://aronsc26:diwa1234@wdc028-course-booking.sysnpgd.mongodb.net/b190-Course-Booking?retryWrites=true&w=majority",
	
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
	);

let db = mongoose.connection;

db.on("error", console.error.bind(console,"connection error"));
db.once("open", () => console.log("we're connected to the database"));

//allows all resources to access our backend application
app.use(cors());

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users",userRoutes);
app.use("/courses",courseRoutes);


//process.env.PORT handles the environment of the hosting websites should app be hosted in a website such as Heroku
app.listen(process.env.PORT || 4000, () => {console.log(`API now online at port ${process.env.PORT || 4000}`)});
