const express = require("express");

const router = express.Router();


const userController = require("../controllers/userController.js");

const auth = require("../auth.js");

//Route for checking if the user's email already exists in the database
//since we will pass a "body" from the request object, post method will be used as HTTP method even if the goal is the just check the database if there is a user email save in it.
router.post("/checkEmail",(req,res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

//Route for user registration
router.post("/register",(req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

//Route for user authentication
router.post("/login",(req,res)=>{
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

//ACTIVITY - Create a /details route that will accept the user’s Id to retrieve the details of a user.

router.post("/details",(req,res)=>{
	userController.getProfile(req.params.id).then(resultFromController => res.send(resultFromController));
})

router.get("/details",auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
})

//route for user enrollment

router.post('/enroll', auth.verify, (req,res) =>{
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}
			userController.enroll(data).then(resultFromController => res.send(resultFromController));	
	})


module.exports = router;